import { PenghitungMundur32Page } from './app.po';

describe('penghitung-mundur32 App', () => {
  let page: PenghitungMundur32Page;

  beforeEach(() => {
    page = new PenghitungMundur32Page();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
