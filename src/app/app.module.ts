import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import {HomeninputdateComponent} from "./homeninputdate/homeninputdate.component";
import {TimerwaktuComponent} from "./timerwaktu/timerwaktu.component";
import {AppRoutingModule} from "./routerdata/app.routers.module";
import {ServicestoreService} from "./services/servicestore.service";

@NgModule({
  declarations: [
    AppComponent,
    HomeninputdateComponent,
    TimerwaktuComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule
  ],
  providers: [ServicestoreService],
  bootstrap: [AppComponent]
})
export class AppModule { }
