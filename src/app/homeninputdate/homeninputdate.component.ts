import {Component, OnInit} from "@angular/core";
import * as moment from "moment";
import {Observable} from "rxjs";
import {Milidetik15Menit} from "../konstans/konstanta";
import {ServicestoreService} from "../services/servicestore.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-homeninputdate',
  templateUrl: './homeninputdate.component.html',
  styleUrls: ['./homeninputdate.component.css']
})
export class HomeninputdateComponent implements OnInit {

  nama_event_input: string = "";
  date_tanggal_input: string = "";
  date_bulan_input: string = "";
  date_tahun_input: string = "";

  date_jam_input: string = "";
  date_menit_input: string = "";

  tanggalkonversi: string = "";

  momenDate: any;
  miliDetik15menit: number = Milidetik15Menit;

  constructor(private serviceStore: ServicestoreService, private route: Router) {
  }

  ngOnInit() {

    //setel tanggal secara global, regional indonesia
    moment.locale('id');
    this.initTimer();
  }

  //inisialisasi waktu timer dengan keterangan 15 menit setelahnya
  initTimer() {

    let milidetik15menit = moment().valueOf() + this.miliDetik15menit;
    this.momenDate = moment(milidetik15menit);

    //tes tanggal lokal
    let locales = moment(milidetik15menit).locale('id');
    // console.log(locales.format('DD MMMM YYYY'));

    let observableInitTanggal = Observable.create(
      observer => {
        let tanggalsformat = this.momenDate.format('DD');
        observer.next(tanggalsformat);
        observer.complete();
      }
    );

    let observableInitBulan = Observable.create(
      observer => {
        let tanggalsformat = this.momenDate.format('MMMM');
        observer.next(tanggalsformat);
        observer.complete();
      }
    );

    let observableInitTahun = Observable.create(
      observer => {
        let tanggalsformat = this.momenDate.format('YYYY');
        observer.next(tanggalsformat);
        observer.complete();
      }
    );

    let observableInitWaktuJam = Observable.create(
      observer => {
        let jamtanggal = this.momenDate.format('HH');
        observer.next(jamtanggal);
        observer.complete();
      }
    );

    let observableInitWaktuMenit = Observable.create(
      observer => {
        let jamtanggal = this.momenDate.format('mm');
        observer.next(jamtanggal);
        observer.complete();
      }
    );

    let observabelZip = Observable.zip(
      observableInitTanggal, observableInitBulan, observableInitTahun, observableInitWaktuJam, observableInitWaktuMenit);

    let subscriber = observabelZip.subscribe(
      (hasils) => {
        this.setDataTanggalInit(hasils);
      },
      (error) => {
        console.log(error);
        this.serviceStore.showSnackbar("Gagal menyetel tanggal pertama kali");
      }
    );

    subscriber.unsubscribe();
  }

  //set data hasil inisialisasi awal
  setDataTanggalInit(vals): void {

    this.date_tanggal_input = vals[0];
    this.date_bulan_input = vals[1];
    this.date_tahun_input = vals[2];
    this.date_jam_input = vals[3];
    this.date_menit_input = vals[4];
  }

  startTimerCountDownCek(): void {

    //12 Februari 2017 13:15
    this.tanggalkonversi = this.date_tanggal_input + " " +
      this.date_bulan_input + " " + this.date_tahun_input + " " +
      this.date_jam_input + ":" + this.date_menit_input;

    //cek tanggal
    //minimal 15 menit sebelum waktu habis
    //cek apakah tanggal yang dimasukkan kurang  dari tanggal sekarang atau tidak
    let observableMsTanggal = this.serviceStore.getServiceOlahTanggal(this.tanggalkonversi);

    let subscription = observableMsTanggal.subscribe(
      (data) => (this.setStartTimerPindahHalaman(data)),
      (error) => {
        console.log("" + error);
        this.serviceStore.showSnackbar("Isi waktu penghitung mundur dengan benar");
      }
    );

    subscription.unsubscribe();

  }

  refreshHalaman(): void {

    this.route.navigate(['/setuptime']);
    location.reload(true);
  }

  //setel data hasil cek timer
  setStartTimerPindahHalaman(value: string) {

    if (this.nama_event_input && this.nama_event_input.length > 0 && this.nama_event_input !== undefined) {
      if (value.length > 1 && value !== undefined && value !== null && value !== "0") {

        //simpan data ke dalam state
        this.serviceStore.setNamaEventCountDown(this.nama_event_input);
        this.serviceStore.setTanggalMs(value);

        //pindah halaman
        this.route.navigate(['/countdowns']);
      }
      else {
        this.serviceStore.showSnackbar("Isi waktu penghitung mundur dengan benar");
      }
    }
    else {
      this.serviceStore.showSnackbar("Isi nama acara hitung waktu mundur dengan benar");
    }
  }


}
