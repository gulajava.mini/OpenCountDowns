/**
 * Created by kucingmint on 2/14/17.
 */
import {NgModule} from "@angular/core";
import {Routes, RouterModule} from "@angular/router";
import {HomeninputdateComponent} from "../homeninputdate/homeninputdate.component";
import {TimerwaktuComponent} from "../timerwaktu/timerwaktu.component";

const APP_ROUTES: Routes = [
  {path: "setuptime", component: HomeninputdateComponent},
  {path: "countdowns", component: TimerwaktuComponent},
  {path: "", redirectTo: "/setuptime", pathMatch: "full"}
];

@NgModule({
  imports: [RouterModule.forRoot(APP_ROUTES)],
  exports: [RouterModule]
})

export class AppRoutingModule {

}
