/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { ServicestoreService } from './servicestore.service';

describe('ServicestoreService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ServicestoreService]
    });
  });

  it('should ...', inject([ServicestoreService], (service: ServicestoreService) => {
    expect(service).toBeTruthy();
  }));
});
