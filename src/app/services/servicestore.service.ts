import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import * as moment from "moment";

@Injectable()
export class ServicestoreService {

  private _tanggalMs: string = "";

  private _namaEventCountDown: string = "My CountDown Event";

  dataTanggalInput: string = "";
  dataJamInput: string = "";
  tanggalkonversi: string = "";

  constructor() {
  }


  //ambil data tanggal dan konversi ke bentuk milidetik string dengan observable
  getServiceOlahTanggal(inputTanggal: string): any {

    //setel tanggal secara global, regional indonesia
    moment.locale('id');

    //12 Februari 2017 13:15
    this.tanggalkonversi = inputTanggal;

    return Observable.of(this.tanggalkonversi)
      .map(
        (tanggalString) => {

          let susunanWaktuMs = 0;

          //pake locale bahasa inggris
          let momenkonversi = moment(tanggalString, "DD MMMM YYYY HH:mm");

          if (momenkonversi) {

            let milidetikBaru = momenkonversi.valueOf();

            //ambil milidetik sekarang
            let milidetikWaktuSekarang = moment().valueOf();

            //jika milidetik baru untuk countdown lebih panjang dari milidetik sekarang
            if (milidetikBaru > milidetikWaktuSekarang) {
              susunanWaktuMs = milidetikBaru;
            }
          }
          return susunanWaktuMs + "";
        }
      )
      .catch(
        (error) => (Observable.throw(error))
      );
  }


  //konversi tanggal dari service dan buat halaman coundown
  getKonversiTanggalEvent(valString: string): any {

    return Observable.of(valString)
      .map(
        (tanggalString) => {

          //April 25, 2016 17:15:00

          // console.log('tanggal ms string ' + tanggalString);

          let valLongTanggal = parseInt(tanggalString);

          let localemomenEN = moment(valLongTanggal).locale('en');

          let susunanWaktuTanggal: string = localemomenEN.format("MMMM DD, YYYY HH:mm:ss");

          // console.log('tanggal hasil ' + susunanWaktuTanggal);

          if (susunanWaktuTanggal) {
            return susunanWaktuTanggal;
          }
          else {
            return "";
          }
        }
      )
      .catch(
        (error) => (Observable.throw(error))
      );
  }


  getTanggalMs(): string {
    return this._tanggalMs;
  }

  setTanggalMs(value: string) {
    this._tanggalMs = value;
  }

  getNamaEventCountDown(): string {
    return this._namaEventCountDown;
  }

  setNamaEventCountDown(value: string) {
    this._namaEventCountDown = value;
  }

  showSnackbar(message : string) : void {

    let x = document.getElementById("snackbar");
    x.textContent = message;
    x.className = "show";

    setTimeout(
      () => {
        x.className = x.className.replace("show", "");
        x.textContent = "";
      }, 3000
    )
  }
}
