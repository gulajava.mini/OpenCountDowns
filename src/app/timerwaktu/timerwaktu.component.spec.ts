/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { TimerwaktuComponent } from './timerwaktu.component';

describe('TimerwaktuComponent', () => {
  let component: TimerwaktuComponent;
  let fixture: ComponentFixture<TimerwaktuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TimerwaktuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TimerwaktuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
