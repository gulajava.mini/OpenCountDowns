import {Component, OnInit, OnDestroy} from "@angular/core";
import {ServicestoreService} from "../services/servicestore.service";
import {Router} from "@angular/router";
import * as moment from "moment";

@Component({
  selector: 'app-timerwaktu',
  templateUrl: './timerwaktu.component.html',
  styleUrls: ['./timerwaktu.component.css']
})
export class TimerwaktuComponent implements OnInit, OnDestroy {

  namaEventCountdown: string = "";
  waktuMilidetik: string = "";
  tanggalKonversiString: string = "";

  dateTanggal: any;
  timerInterval: any;
  elementAnimasiClock: any;

  objectTimer = {
    "days": 0,
    "hours": 0,
    "minutes": 0,
    "seconds": 0,
    "totals": 0
  };

  constructor(private serviceStore: ServicestoreService,
              private routers: Router) {

    //setel tanggal secara global, regional indonesia
    moment.locale('id');
  }

  ngOnInit() {

    this.getDataTanggal();
    this.konversiTanggalKeFormatDateString();
  }

  ngOnDestroy(): void {

    this.stopTimer();
  }

  getDataTanggal(): void {

    this.namaEventCountdown = this.serviceStore.getNamaEventCountDown().toUpperCase();
    this.waktuMilidetik = this.serviceStore.getTanggalMs();
  }

  konversiTanggalKeFormatDateString(): void {

    let observableKonvertTgl = this.serviceStore.getKonversiTanggalEvent(this.waktuMilidetik);

    observableKonvertTgl.subscribe(
      (hasil) => {
        this.tanggalKonversiString = hasil;
        this.setCekDataKonversi();
      },
      (error) => {
        console.log(error);
        this.tampilNotifikasiError("Gagal menghitung waktu mundur");
      }
    )
  }

  setCekDataKonversi(): void {

    if (this.tanggalKonversiString.length > 1) {

      let promiseArrowTanggal = new Promise(
        (resolve, reject) => {

          let date = new Date(this.tanggalKonversiString);
          resolve(date);
        }
      );

      promiseArrowTanggal.then(
        (tanggalDate) => {
          this.dateTanggal = tanggalDate;
          this.startTimer();
        }
      )
        .catch(
          (error) => {
            console.log(error.toString() + "");
            this.tampilNotifikasiError("Gagal menghitung waktu mundur");
          }
        )
    }
    else {
      this.tampilNotifikasiError("Gagal menghitung waktu mundur");
    }
  }

  updateTimerWaktu(): void {

    //kalkulasi tanggal dan waktu mundur nya
    let dateSekarang: any = new Date();
    let timeWaktuSelisih = this.dateTanggal - dateSekarang;

    let days = Math.floor(timeWaktuSelisih / (1000 * 60 * 60 * 24));
    let hours = Math.floor((timeWaktuSelisih / (1000 * 60 * 60)) % 24);
    let minutes = Math.floor((timeWaktuSelisih / 1000 / 60) % 60);
    let seconds = Math.floor((timeWaktuSelisih / 1000) % 60);
    let total = timeWaktuSelisih;

    this.objectTimer.days = days;
    this.objectTimer.hours = hours;
    this.objectTimer.minutes = minutes;
    this.objectTimer.seconds = seconds;
    this.objectTimer.totals = total;
  }

  startTimer(): void {

    this.timerInterval = setInterval(
      () => {

        this.elementAnimasiClock = document.getElementById("clock");

        //kalkulasi tanggal dan waktu mundur nya
        this.updateTimerWaktu();

        //cek nan
        if (isNaN(this.objectTimer.days) || isNaN(this.objectTimer.hours) || isNaN(this.objectTimer.minutes) ||
          isNaN(this.objectTimer.seconds)) {
          this.navigasiKeHalamanSetelTanggal();
        }

        let spans = this.elementAnimasiClock.getElementsByTagName("span");

        //animasikan tanggal
        this.animasiTanggal(spans[3]);

        if (this.objectTimer.seconds == 59) {
          this.animasiTanggal(spans[2]);
        }

        if (this.objectTimer.minutes == 59 && this.objectTimer.seconds == 59) {
          this.animasiTanggal(spans[1]);
        }

        if (this.objectTimer.hours == 23 && this.objectTimer.minutes == 59 && this.objectTimer.seconds == 59) {
          this.animasiTanggal(spans[0]);
        }

        if (this.objectTimer.totals < 1) {
          this.stopTimer();
          //reset data timer
          this.objectTimer.days = 0;
          this.objectTimer.hours = 0;
          this.objectTimer.minutes = 0;
          this.objectTimer.seconds = 0;
        }

      }, 1000
    );
  }

  stopTimer(): void {

    let stopPromise = new Promise(
      (resolve, reject) => {
        clearInterval(this.timerInterval);
        resolve("")
      }
    );

    stopPromise.then(
      () => {
      }
    )
      .catch(
        (error) => {
          console.log(error);
        }
      )
  }

  animasiTanggal(span: any): void {
    span.className = "turn";
    setTimeout(
      () => {
        span.className = "";
      }, 700
    );
  }

  tampilNotifikasiError(val: string): void {

    //tampil alert dan pindah ke halaman sebelumnya
    this.serviceStore.showSnackbar(val);
  }

  navigasiKeHalamanSetelTanggal() {

    this.routers.navigate(["/setuptime"])
  }

}
